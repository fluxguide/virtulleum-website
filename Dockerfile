# Use an official Ubuntu Xenial as a parent image
FROM ubuntu:16.04

# Install Node.js 8 and npm 5
RUN apt-get update
RUN apt-get -qq update
RUN apt-get install -y build-essential
RUN apt-get install -y curl
RUN apt-get install -y git
RUN curl -sL https://deb.nodesource.com/setup_10.x | bash
RUN apt-get install -y nodejs

# Set the working directory to /app
WORKDIR /app

# Copy the current directory contents into the container
ADD . /app

# add the real package json file
ADD ./package_ORIGINAL.json /app/package.json

# Install any needed packages specified in requirements.txt
#RUN npm install

# Copy the specific node module to the container (because martins personal git repo does not work)
#COPY gatsby-plugin-remote-images /app/node_modules/gatsby-plugin-remote-images

# Run `npm start` when the container launches
#CMD ["npm", "run", "build"]

# Copy the entrypoint shell script to the container
COPY entrypoint.sh /app/entrypoint.sh
# Make the shell script executable
RUN chmod +x /app/entrypoint.sh
# Set the entrypoint to the shell script
ENTRYPOINT ["/app/entrypoint.sh"]



