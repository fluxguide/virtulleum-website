# Virtulleum Website 




# NEW Method (with docker)

- install docker 
- make "./run_docker.sh" executable (e.g. "chmod +x ./run_docker.sh")
- run "run_docker.sh"
- now the "public"-folder will be created. This folder conatins the static HTML-App and can then be deployed on a webserver.





# OLD Method (with Netlify)


[![Netlify Status](https://api.netlify.com/api/v1/badges/853f7e7e-2b76-4351-bec9-087559f13be4/deploy-status)](https://app.netlify.com/sites/virtulleum-website-preview/deploys)


## Prerequisites

- node
- npm

## Installation

```
npm i
```

## Run for development

```
npm run dev
```

This will start the development server and open your browser at http://localhost:8000

## Build for production

```
npm run build
```

Create a production build. 

## Deployment

As soon as you push a commit to "master" branch, the website will be deployed on netlify.