module.exports = {
    siteMetadata: {
        title: `Virtulleum`,
        description: `Sie stehen am Beginn Ihrer spannenden Expedition auf den Spuren der Tullner Stadtgeschichte(n). Eine App führt Sie mit Ihren „Geschichtswürfeln“ auf dieser Reise. Lassen Sie sich auf Ihre persönliche Stadtexpedition ein!`,
        author: `fluxguide`,
        siteUrl: 'https://www.virtulleum.at'
    },
    plugins: [
        {
          resolve: `gatsby-plugin-gdpr-cookies`,
          options: {
            googleAnalytics: {
              trackingId: 'G-MN3QM782H6',
              // Setting this parameter is optional
              anonymize: true
            },
            // Defines the environments where the tracking should be available  - default is ["production"]
            environments: ['production', 'development']
          },
        },
        `gatsby-plugin-react-helmet`,
        `gatsby-plugin-robots-txt`,
        // `gatsby-plugin-flow`,
        {
            resolve: `gatsby-source-filesystem`,
            options: {
                name: `images`,
                path: `${__dirname}/src/`
            }
        },
        {
          resolve: `gatsby-source-apiserver`,
          options: {

            headers: {
              "Content-Type": "application/json"
            },

            entitiesArray: [
              {
                url: `https://virtulleum.fluxguide.com/fluxguide/exhibitions/1/system/ci_controllers/exhibition/get_welcome_section`,
                name: `welcomesection`
              },
              {
                url: `https://virtulleum.fluxguide.com/fluxguide/exhibitions/1/system/ci_controllers/exhibition/get_virtulleum_questions`,
                name: `questions`
              },
              {
                url: `https://virtulleum.fluxguide.com/fluxguide/exhibitions/1/system/ci_controllers/exhibition/get_category_stops_list/`,
                name: `category_stops`
              },
              {
                url: `https://virtulleum.fluxguide.com/fluxguide/exhibitions/1/system/ci_controllers/exhibition/get_visitor_infos/`,
                name: `visitor_infos`
              },
              {
                url: `https://virtulleum.fluxguide.com/fluxguide/exhibitions/1/system/ci_controllers/exhibition/get_virtulleum_faqs/`,
                name: `faqs`
              },
              {
                url: `https://virtulleum.fluxguide.com/fluxguide/exhibitions/1/system/ci_controllers/exhibition/get_town_history_list/`,
                name: `town_history_list`
              },
              {
                url: `https://virtulleum.fluxguide.com/fluxguide/exhibitions/1/system/ci_controllers/exhibition/get_archive_list/`,
                name: `archive_list`
              },
              {
                url: `https://virtulleum.fluxguide.com/fluxguide/exhibitions/1/system/ci_controllers/exhibition/get_impressum/`,
                name: `impressum`
              },
              {
                url: `https://virtulleum.fluxguide.com/fluxguide/exhibitions/1/system/ci_controllers/exhibition/get_bibliography/`,
                name: `bibliography`
              },
              {
                url: `https://virtulleum.fluxguide.com/fluxguide/exhibitions/1/system/ci_controllers/exhibition/get_privacy/`,
                name: `privacy`
              },
              {
                url: `https://virtulleum.fluxguide.com/fluxguide/exhibitions/1/system/ci_controllers/exhibition/get_news/`,
                name: `news`
              },
              {
                url: `https://virtulleum.fluxguide.com/fluxguide/exhibitions/1/system/ci_controllers/exhibition/get_about_project_virtulleum_infos/`,
                name: `about_project_virtulleum_infos`
              },
              {
                url: `https://virtulleum.fluxguide.com/fluxguide/exhibitions/1/system/ci_controllers/exhibition/get_special_exhibition/`,
                name: `special_exhibition`
              }
            ]

          },
        },
        {
          resolve: `gatsby-plugin-remote-images`,
          options: {
            nodeType: `welcomesection`,
            imagePath: `image_url`,
            name: `welcomeImage`
          },
        },
        {
          resolve: `gatsby-plugin-remote-images`,
          options: {
            nodeType: `category_stops`,
            imagePath: `categories[].stops[].virtulleum_website_image_thumb`,
            name: `virtulleumWebsiteImageThumb`
          },
        },
        {
          resolve: `gatsby-plugin-remote-images`,
          options: {
            nodeType: `special_exhibition`,
            imagePath: `image_url`,
            name: `specialExhibitionImage`
          },
        },
        // {
        //     resolve: `gatsby-plugin-remote-images`,
        //     options: {
        //       nodeType: `category_stops`,
        //       imagePath: `categories[].stops[].virtulleum_website_images[].url`,
        //       name: `virtulleumWebsiteImages`
        //     },
        // },
        `gatsby-plugin-sharp`,
        `gatsby-transformer-sharp`,
        /*{
            resolve: `gatsby-plugin-manifest`,
            options: {
                name: `gatsby-starter-default`,
                short_name: `starter`,
                start_url: `/`,
                background_color: `#FFFFFF`,
                theme_color: `#FFFFFF`,
                display: `minimal-ui`,
                icon: `src/images/virtulleum_icon.png` // This path is relative to the root of the site.
            }
        },*/
        {
            resolve: "gatsby-plugin-i18n",
            options: {
                availableLngs: ['en', 'de'],
                fallbackLng: 'de',
                langKeyDefault: "de",
                useLangKeyLayout: false
            }
        }
    ]
};
