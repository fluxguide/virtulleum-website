"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");
var _objectWithoutPropertiesLoose2 = _interopRequireDefault(require("@babel/runtime/helpers/objectWithoutPropertiesLoose"));
var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));
const _excluded = ["name"];
const _require = require(`gatsby-source-filesystem`),
  createRemoteFileNode = _require.createRemoteFileNode;
const get = require('lodash/get');
exports.onCreateNode = /*#__PURE__*/function () {
  var _ref = (0, _asyncToGenerator2.default)(function* ({
    node,
    actions,
    store,
    cache,
    createNodeId
  }, options) {
    const createNode = actions.createNode;
    const nodeType = options.nodeType,
      imagePath = options.imagePath,
      _options$name = options.name,
      name = _options$name === void 0 ? 'localImage' : _options$name,
      _options$auth = options.auth,
      auth = _options$auth === void 0 ? {} : _options$auth,
      _options$ext = options.ext,
      ext = _options$ext === void 0 ? null : _options$ext;
    const createImageNodeOptions = {
      store,
      cache,
      createNode,
      createNodeId,
      auth,
      ext,
      name
    };
    if (node.internal.type === nodeType) {
      // Check if any part of the path indicates the node is an array and splits at those indicators
      let imagePathSegments = [];
      if (imagePath.includes("[].")) {
        imagePathSegments = imagePath.split("[].");
      }
      if (imagePathSegments.length) {
        yield createImageNodesInArrays(imagePathSegments[0], node, Object.assign({
          imagePathSegments
        }, createImageNodeOptions));
      } else {
        const url = getPath(node, imagePath, ext);
        yield createImageNode(url, node, createImageNodeOptions);
      }
    }
  });
  return function (_x, _x2) {
    return _ref.apply(this, arguments);
  };
}();

// Returns value from path, adding extension when supplied
function getPath(node, path, ext = null) {
  const value = get(node, path);
  return ext ? value + ext : value;
}

// Creates a file node and associates the parent node to its new child
function createImageNode(_x3, _x4, _x5) {
  return _createImageNode.apply(this, arguments);
} // Recursively traverses objects/arrays at each path part, then operates on targeted leaf node
function _createImageNode() {
  _createImageNode = (0, _asyncToGenerator2.default)(function* (url, node, options) {
    const name = options.name,
      restOfOptions = (0, _objectWithoutPropertiesLoose2.default)(options, _excluded);
    let fileNode;
    if (!url) {
      return;
    }
    try {
      fileNode = yield createRemoteFileNode(Object.assign({}, restOfOptions, {
        url,
        parentNodeId: node.id
      }));
    } catch (e) {
      console.error('gatsby-plugin-remote-images ERROR:', e);
    }
    // Adds a field `localImage` or custom name to the node
    // ___NODE appendix tells Gatsby that this field will link to another node
    if (fileNode) {
      node[`${name}___NODE`] = fileNode.id;
    }
  });
  return _createImageNode.apply(this, arguments);
}
function createImageNodesInArrays(_x6, _x7, _x8) {
  return _createImageNodesInArrays.apply(this, arguments);
}
function _createImageNodesInArrays() {
  _createImageNodesInArrays = (0, _asyncToGenerator2.default)(function* (path, node, options) {
    if (!path || !node) {
      return;
    }
    const imagePathSegments = options.imagePathSegments,
      ext = options.ext;
    const pathIndex = imagePathSegments.indexOf(path),
      isPathToLeafProperty = pathIndex === imagePathSegments.length - 1,
      nextValue = getPath(node, path, isPathToLeafProperty ? ext : null);

    // grab the parent of the leaf property, if it's not the current value of `node` already
    // ex: `parentNode` in `myNodes[].parentNode.leafProperty`
    let nextNode = node;
    if (isPathToLeafProperty && path.includes('.')) {
      const pathToLastParent = path.split('.').slice(0, -1).join('.');
      nextNode = get(node, pathToLastParent);
    }
    return Array.isArray(nextValue)
    // Recursively call function with next path segment for each array element
    ? Promise.all(nextValue.map(item => createImageNodesInArrays(imagePathSegments[pathIndex + 1], item, options)))
    // otherwise, handle leaf node
    : createImageNode(nextValue, nextNode, options);
  });
  return _createImageNodesInArrays.apply(this, arguments);
}