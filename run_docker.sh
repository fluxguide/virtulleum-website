#!/bin/bash

# unzip the prebuild node-modules folder
rm -rf ./node_modules
rm -rf ./node_modules_original
unzip -o ./node_modules_original.zip -d ./ -x "__MACOSX/*"
mv ./node_modules_original ./node_modules

# delete existing "public"-folder (only if exists)
rm -rf ./public
if [ -d "$(pwd)"/public ]; then
    echo "Deleting $folder..."
    rm -rf "$(pwd)"/public
fi

# create docker-image (only if it not exists yet)
if [[ "$(docker images -q ubuntu-16-virtulleum-image 2> /dev/null)" == "" ]]; then
    echo "Building image 'ubuntu-16-virtulleum-image'..."
    docker build -t ubuntu-16-virtulleum-image .
else
    echo "Image 'ubuntu-16-virtulleum-image' already exists. Skipping build."
fi

# delete docker container (if already exists)
if docker inspect virtulleum-website-ubuntu-16-container >/dev/null 2>&1; then
  echo "Deleting existing container"
  docker rm virtulleum-website-ubuntu-16-container
fi

# start docker-container
echo "Starting container..."
# docker run -d -it --name virtulleum-website-ubuntu-16-container -v "$(pwd)"/public:/app/public ubuntu-16-virtulleum-image
docker run --rm --name virtulleum-website-ubuntu-16-container -v "$(pwd)"/public:/app/public ubuntu-16-virtulleum-image

echo "EVERYTHING IS READY!! Virtuelleum-Website has been created. You will find your www-files in /public."