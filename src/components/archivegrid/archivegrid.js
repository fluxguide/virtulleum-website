import React, { useState } from "react";
import { useStaticQuery, graphql } from "gatsby";

import { Dialog } from "@reach/dialog";
import "@reach/dialog/styles.css";
import VisuallyHidden from "@reach/visually-hidden";

import {cloneDeep, forEach, filter} from 'lodash';

const ArchiveGrid = props => {

    // define urls
    const FG_BASE_URL = "https://virtulleum.fluxguide.com/fluxguide/";
    const data = useStaticQuery(ARCHIVE_DATA);

    const language_id = props.lang_id;

    let archive = filter( cloneDeep(data.allArchiveList.nodes[0].archive), function(el){ return el.sys_languages_id == language_id});
    let archive_cats = filter( cloneDeep(data.allArchiveList.nodes[0].categories), function(el){ return el.language_id == language_id});

    forEach(archive_cats, function(cat, index){
        let archive_items = filter(archive, {virtulleum_website_archive_cat_id : cat.category_id});
        cat.archive_items = archive_items;
    });

    const [showDialog, setShowDialog] = useState(false);
    const [selectedStop, setSelectedStop] = useState(null);

    // click on stop -> set selected stop and open dialog
    let archiveObjectClick = my_selected_stop => {
        setSelectedStop(my_selected_stop);
        setShowDialog(true);
    };

    const translations = {
        all_videos : {
            1: "All Videos",
            2: "Alle Videos"
        }
    }

    const [selectbox, setSelectbox] = useState("all");
    const [catTitle, setCatTitle] = useState( translations.all_videos[language_id] );

    const handleAddrTypeChange = (e, index) => {
        setSelectbox(e.target.value);
    }
    
    const changeCat = (index, category_title) => {
        setSelectbox(index);
        setCatTitle(category_title);
    }
    

    return (
        <>  

            <div className="select-box">
                <div className="select-box__current" tabIndex="1">
                    <div className="select-box__value">
                        <p className="select-box__input-text">{catTitle}</p>
                    </div>

                    <svg xmlns="http://www.w3.org/2000/svg" className="arrow_icon" width="10.121" height="17.414" viewBox="0 0 10.121 17.414">
                        <path d="M0,8,8,0l8,8" transform="translate(8.707 0.707) rotate(90)" />
                    </svg>
                </div>
                <ul className="select-box__list">

                    <li value="all" onClick={() => { changeCat("all", translations.all_videos[language_id] ); }}>
                        <p className="select-box__option" aria-hidden="aria-hidden">{translations.all_videos[language_id]}</p>
                    </li>

                    {archive_cats.map((archive_cat, index) => (
                        <li key={index} value={index} onClick={() => { changeCat(index, archive_cat.category_title); }}>
                            <p className="select-box__option" aria-hidden="aria-hidden">{archive_cat.category_title}</p>
                        </li>
                    ))}

                </ul>
            </div>

            
            {/* <div className="custom_select_archive">
                <select className="archive_select" onChange={e => handleAddrTypeChange(e)}>
                    <option value="all">{translations.all_videos[language_id]}</option>

                    {archive_cats.map((archive_cat, index) => (
                        <option key={index} value={index}>{archive_cat.category_title}</option>
                    ))}
                </select>
                <svg xmlns="http://www.w3.org/2000/svg" className="arrow_icon" width="10.121" height="17.414" viewBox="0 0 10.121 17.414">
                    <path d="M0,8,8,0l8,8" transform="translate(8.707 0.707) rotate(90)" />
                </svg>
            </div> */}

            {
                archive_cats.map((archive_cat, index) => {
                    if(archive_cat.archive_items.length == 0) return null; 

                    return (
                        <div className={`active_cat ${selectbox == index ? "selected" : ""} ${selectbox == "all" ? "all_selected" : ""} `}  key={index} >
                            <h3>{archive_cat.category_title}</h3>
                            <section className="archive_section">

                                {archive_cat.archive_items.map((archive_item, index) => (

                                    <article
                                        key={index}
                                        className="archive_item"
                                        data-link={archive_item.virtulleum_website_url}
                                        onClick={() => {
                                            archiveObjectClick(archive_item);
                                        }}
                                    >
                                        <img
                                            src={`${FG_BASE_URL}${archive_item.images[0]}`}
                                            alt={archive_item.title}
                                        />

                                        <h4>{archive_item.title}</h4>

                                        <button className="play_btn">
                                            <div className="play_btn_bg"></div>
                                            <svg
                                                xmlns="http://www.w3.org/2000/svg"
                                                className="play_icon"
                                                viewBox="0 0 13.333 20"
                                            >
                                                <path d="M1.111,20H.667A1.222,1.222,0,0,1,0,18.889V1.111A1.222,1.222,0,0,1,.667,0,1.665,1.665,0,0,1,1.778.222L12.889,9.111a1.111,1.111,0,0,1,0,1.778L1.778,19.778A.816.816,0,0,1,1.111,20Z" />
                                            </svg>
                                        </button>
                                    </article>

                                ))}

                            </section>
                        </div>
                    )
                })
            }


            {selectedStop && (
                <Dialog
                    isOpen={showDialog}
                    onDismiss={() => setShowDialog(false)}
                >
                    <button
                        className="close-button"
                        onClick={() => setShowDialog(false)}
                    >
                        <VisuallyHidden>Close</VisuallyHidden>
                        <span aria-hidden>×</span>
                    </button>

                    <iframe width="560" height="315" src={"https://www.youtube-nocookie.com/embed/" + selectedStop.virtulleum_website_url + "?autoplay=1&modestbranding=1&showinfo=0&rel=0"} frameBorder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>
                </Dialog>
            )}
        </>
    );
};

export default ArchiveGrid;



const ARCHIVE_DATA = graphql`
    query ArchiveData {
        allArchiveList {
            nodes {
              archive {
                alternative_id
                description
                images
                title
                virtulleum_website_archive_cat_id
                virtulleum_website_description_more
                virtulleum_website_is_archiv
                virtulleum_website_url
                sys_languages_id
              }
              categories {
                category_id
                category_title
                language_id
              }
            }
          }
    }
`