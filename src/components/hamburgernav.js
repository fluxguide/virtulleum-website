import React from "react";
import { Link } from "gatsby";

const HamburgerNav = props => {

    // set current language
    let language_id = props.lang_id;
    

    let navClass = "hamburger_nav";
    if(props.isOpen === true) navClass += " active";


    const translations = {
        home : {
            1: "Home",
            2: "Home"
        },
        museum_objects : {
            1: "Museum Objects",
            2: "Museumsobjekte"
        },
        your_visit : {
            1: "Your Visit",
            2: "Ihr Besuch"
        },
        town_history : {
            1: "City history",
            2: "Stadtgeschichte"
        }
    }


    return (
        <>  
            {language_id == 2 && (
                <nav className={navClass}>
                    <ul>
                        <button
                            className="hamburger"
                            type="button"
                            data-icon="close_icon"
                            onClick={props.onClose}
                        >
                            <span className="hamburger-box">
                                <span className="hamburger-inner"></span>
                            </span>
                        </button>

                        <li>
                            <Link to="/">{translations.home[language_id]}</Link>
                        </li>

                        <li>
                            <Link to="/#museumsobjekte">{translations.museum_objects[language_id]}</Link>
                        </li>

                        <li>
                            <Link to="/stadtgeschichte">{translations.town_history[language_id]}</Link>
                        </li>

                        <li>
                            <Link to="/besucherinfos">{translations.your_visit[language_id]}</Link>
                        </li>

                        <li className="lang_link">
                           <span><Link to="/en/">EN</Link></span>
                           <span><Link to="/">DE</Link></span>
                        </li>

                    </ul>
                </nav>
            )}

            {language_id == 1 && (
                <nav className={navClass}>
                    <ul>
                        <button
                            className="hamburger"
                            type="button"
                            data-icon="close_icon"
                            onClick={props.onClose}
                        >
                            <span className="hamburger-box">
                                <span className="hamburger-inner"></span>
                            </span>
                        </button>

                        <li>
                            <Link to="/en/">{translations.home[language_id]}</Link>
                        </li>

                        <li>
                            <Link to="/en/#museumsobjekte">{translations.museum_objects[language_id]}</Link>
                        </li>

                        <li>
                            <Link to="/en/stadtgeschichte">{translations.town_history[language_id]}</Link>
                        </li>

                        <li>
                            <Link to="/en/besucherinfos">{translations.your_visit[language_id]}</Link>
                        </li>

                        <li className="lang_link">
                           <span><Link to="/en/">EN</Link></span>
                           <span><Link to="/">DE</Link></span>
                        </li>
                    </ul>
                </nav>
            )}
        </>
    );
};

export default HamburgerNav;
