import React, { useEffect, useState } from "react";
import { useStaticQuery, graphql } from "gatsby";

import Fuse from 'fuse.js';

import { Dialog } from "@reach/dialog";
import VisuallyHidden from "@reach/visually-hidden";
import "@reach/dialog/styles.css";

import Tagify  from "@yaireo/tagify";

import {forEach, union, difference, intersection, filter, orderBy} from 'lodash';

import Swiper from "react-id-swiper";
// import "react-id-swiper/lib/styles/css/swiper.css";

const HistoryGrid = (props) => {

    // get main objects data
    const FG_BASE_URL = "https://virtulleum.fluxguide.com/fluxguide/";
    const data = useStaticQuery(HISTORY_DATA);

    const language_id = props.lang_id;

    let history = filter(data.allTownHistoryList.nodes, function(el){ return el.sys_languages_id == language_id});

    forEach(history, function(item){ item.virtulleum_public_number = parseInt(item.virtulleum_public_number) });

    history = orderBy(history, ['virtulleum_public_number'], ['asc']);

    console.log(history);

    // local state for stops
    const [showDialog, setShowDialog] = useState(false);
    const [selectedHistory, setSelectedHistory] = useState();

    let [filteredHistory, setFilteredHistory] = useState(history);
  
    let mainObjectClick = my_selected_stop => {
        setSelectedHistory(my_selected_stop);
        setShowDialog(true);
    };


    const swiperParams = {
        pagination: {
            el: ".swiper-pagination",
            type: "bullets",
            clickable: true
        },
        loop: true,
        autoplay: {
            delay: 3000
        },
        zoom: {
            maxRatio: 3
        }
    };


    // intialize fuse
    var fuse_options = {
        shouldSort: true,
        tokenize: true,
        findAllMatches: true,
        threshold: 0.3,
        location: 0,
        distance: 1000,
        maxPatternLength: 100,
        minMatchCharLength: 1,
        keys: [
            "virtulleum_website_tags"
        ]
    };
    var fuse = new Fuse(history, fuse_options);


    //   "title",
    //   "description",
    //   "virtulleum_website_description_more",

    var filter_new_history = function(){

        // Get tagify values
        var tags = window.tagify.value;

        // var tagify_value = "";
        // forEach(tags, function(tag, key){
        //     tagify_value += " " + tag.value;
        // });
        
        // Get input value
        // var input_value = document.querySelector('.text_input').value;
        
        // // Merge both values for the fuzzy search
        // var search_value = tagify_value + " " + input_value;
 
        // var search_value = window.tagify.DOM.input.innerText.replace(/(\r\n|\n|\r)/gm, " ");

        var tagify_values = [];
        forEach(tags, function(tag, key){
            tagify_values.push(tag.value);
        });

        var result_history = [];

        if(tagify_values.length > 0){
            // var result_history = fuse.search(tagify_value);
            
            forEach(history, function(obj){
                if(  difference(tagify_values, obj.virtulleum_website_tags).length === 0 ) result_history.push(obj);
            });

            setFilteredHistory(result_history);
        } else {
            setFilteredHistory(history);
        }
    }

    /**
     * Search through all stops
     */
    let search = function(event) {
        // let search_term = event.currentTarget.value;
        filter_new_history();
    }
    
    // init Tagify script on the above inputs
    useEffect(function() {
        
        let tags = [];
        forEach(history, function(value, key){
            tags = union(tags, value.virtulleum_website_tags).sort();
        });

        var tagify_settings = {
            whitelist : tags,
            maxTags: 10,
            // mode: 'mix',
            // pattern: /#/,
            // pattern: /@|#/,
            enforceWhitelist: true,
            dropdown: {
              maxItems: 999999,           // <- maxumum allowed rendered suggestions
              classname: "tags-look", // <- custom classname for this dropdown, so it could be targeted
              enabled: 0,             // <- show suggestions on focus
              closeOnSelect: false    // <- do not hide the suggestions dropdown once an item has been selected
            }
        };

        let input = document.querySelector('.tags_input');
        window.tagify = new Tagify(input, tagify_settings);


        // add new Tag
        window.tagify.on('add', function(e){
            let new_tag = e.detail.data.value;

            if(new_tag.length > 0) {
                filter_new_history();
            }

        });

        // remove a Tag
        window.tagify.on('remove', function(e){
            var removed_value = e.detail.tag.title;

            if(removed_value.length > 0){
                filter_new_history();
            }
        });


        // A good place to pull server suggestion list accoring to the prefix/value
        // window.tagify.on('input', function(e){

        //     var prefix = e.detail.prefix;

        //     if( prefix ){
        //         if( e.detail.value.length > 1 ) window.tagify.dropdown.show.call(window.tagify, e.detail.value);
        //     }
            
        //     var text_input = e.detail.textContent;
        //     if(text_input.length > 0) filter_new_history();
       
        //     // console.log( window.tagify.value )
        //     // console.log('mix-mode "input" event value: ', e.detail)
        // });

    

    }, []);


    let createLinkName = link => {
        var link = link.replace(/^.*[\\\/]/, '');
        link = link.replace(/_/g, " ");
        link = link.replace(/-/g, " ");
        link = link.replace(".", " - ");
        console.log(link);
        return link;
    }
    

    const translations = {
        no_results : {
            1: "No Search results",
            2: "Keine Suchergebnisse gefunden"
        },
        tag_search : {
            1: "Search for Tags",
            2: "Suche nach Schlagwörtern"
        },
        other_info : {
            1: "More Information",
            2: "Weiterführende Informationen"
        }
    }
    

    return (
        <>  
            {/* <h3>Verwenden Sie '#' um nach Schlagwörtern zu suchen. (Beispiel: #Kloster)</h3> */}
            <div className="tag_wrapper">
                <section className="tag_search">
                    <input name="input" className="tags_input search_input" placeholder={translations.tag_search[language_id]} />
                </section>
                {/* <input type="search" placeholder="Volltextsuche" onChange={search} className="text_input search_input" /> */}
            </div>

      
            <section className="town_questions_section">
                {filteredHistory.length === 0 && <p>{translations.no_results[language_id]}</p> }

                {filteredHistory.map((history_item, index) => (
                    <button
                        key={index}
                        className="town_question_btn"
                        onClick={() => {
                            mainObjectClick(history_item);
                        }}
                    >
                        <div className="town_question_wrapper" lang="de">

                            {history_item.images.length == 0 && (
                                <div className="image_placeholder">
                                    <svg className="logo_img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 121.632 152.625">
                                        <path d="M26.929,49.158A4.629,4.629,0,1,0,22.3,44.529,4.7,4.7,0,0,0,26.929,49.158Zm0-6.756A2.127,2.127,0,1,1,24.8,44.529,2.192,2.192,0,0,1,26.929,42.4Z" transform="translate(5.63 10.016)"/>
                                        <rect width="3.753" height="2.502" transform="translate(93.734 71.183)"/>
                                        <rect width="2.627" height="2.502" transform="translate(97.237 81.066)"/>
                                        <rect width="4.253" height="2.502" transform="translate(72.967 67.806)"/>
                                        <rect width="6.255" height="2.502" transform="translate(75.719 81.192)"/>
                                        <rect width="5.379" height="2.502" transform="translate(110.497 76.062)"/>
                                        <rect width="5.129" height="2.502" transform="translate(108.621 20.142)"/>
                                        <rect width="3.002" height="2.502" transform="translate(80.848 73.185)"/>
                                        <rect width="6.756" height="2.502" transform="translate(109.372 36.405)"/>
                                        <rect width="5.63" height="2.502" transform="translate(92.358 16.263)"/>
                                        <rect width="4.128" height="2.502" transform="translate(105.994 28.648)"/>
                                        <path d="M120.981,10.759S102.34,0,94.209,0,67.437,10.759,67.437,10.759a1.314,1.314,0,0,0-.626,1.126V29.024A34.987,34.987,0,0,0,37.287,5.254h0a20.269,20.269,0,0,0-2.752-.125h-.751c-10.759,0-19.391,2.627-26.272,4.754a1.2,1.2,0,0,0-.876,1.126V30.775A1.094,1.094,0,0,0,7.387,31.9a1.142,1.142,0,0,0,1.376-.25,29.809,29.809,0,0,1,19.391-8.007h.25a27.283,27.283,0,0,1,7.881,1.126l-.25,2.127A26.1,26.1,0,0,0,9.764,33.653a14.06,14.06,0,0,0-3-.375,1.544,1.544,0,0,0-1,.5A32.112,32.112,0,0,0,.006,47.789,1.1,1.1,0,0,0,.632,49.04c1,.626,2,1.126,3,1.626a34.709,34.709,0,0,0,9.883,3A29.831,29.831,0,0,1,9.889,67.931l-.125.125a35.312,35.312,0,0,1-2.252,3.628c-.626.876-1.251,1.626-1.877,2.377a1.315,1.315,0,0,0,0,1.626c3.253,3.628,8.382,5.63,14.887,5.63a6.8,6.8,0,0,0,6.38-4.253l2.252,1.251V88.322H15.769a1.182,1.182,0,0,0-1.251,1.251v9.758a53.338,53.338,0,0,0,53.294,53.294h.5a53.338,53.338,0,0,0,53.294-53.294V89.573h0V12.01A1.38,1.38,0,0,0,120.981,10.759ZM20.148,116.6h.5c2.5,0,3.753-1.376,4.879-2.5.876-1,1.626-1.751,3-1.751s2.127.751,3,1.751a6.19,6.19,0,0,0,4.879,2.5c2.5,0,3.753-1.376,4.879-2.5.876-1,1.626-1.751,3-1.751s2.127.751,3,1.751a6.19,6.19,0,0,0,4.879,2.5,4.927,4.927,0,0,0,2.5-.626H67.061v12.01a5.446,5.446,0,0,1-2.252-1.626,6.19,6.19,0,0,0-4.879-2.5c-2.5,0-3.753,1.376-4.879,2.5-.876,1-1.626,1.751-3,1.751s-2.127-.751-3-1.751a6.19,6.19,0,0,0-4.879-2.5c-2.5,0-3.753,1.376-4.879,2.5-.876,1-1.626,1.751-3,1.751s-2.127-.751-3-1.751a6.19,6.19,0,0,0-4.879-2.5,5.634,5.634,0,0,0-4,1.626A64.664,64.664,0,0,1,20.148,116.6ZM69.313,46.913h3.5v-2.5h-3.5V13.136H119.1v31.9h-2.127v2.5H119.1V67.555h-5.5v2.5h5.5V88.2H104.842V76.313c.125-.125,8.132-15.638,8.132-15.638a13.273,13.273,0,0,0,1.751-6.38,12.422,12.422,0,0,0-3.378-8.632,11.336,11.336,0,0,0-8.132-3.253h-.626V28.4a4.433,4.433,0,0,0-4.379-4.379H78.821A4.433,4.433,0,0,0,74.442,28.4v2.627H71.69v2.5h2.752v28.4A4.433,4.433,0,0,0,78.821,66.3h8.632V88.072H69.313Zm-1.251,66.68H56.553V90.7H79.572v22.894Zm35.529-40.158L95.585,59.549a10.335,10.335,0,0,1-1.5-5.254c0-5.379,3.878-9.508,9.007-9.508h0a8.686,8.686,0,0,1,6.38,2.627,10.071,10.071,0,0,1,2.752,6.881,10.706,10.706,0,0,1-1.5,5.254ZM91.707,53.544H82.074V32.151H94.959V45.788A12.532,12.532,0,0,0,91.707,53.544Zm8.382-10.759a11.876,11.876,0,0,0-2.627,1.126V30.9a1.182,1.182,0,0,0-1.251-1.251H80.823A1.182,1.182,0,0,0,79.572,30.9V54.795a1.182,1.182,0,0,0,1.251,1.251H91.832A13.411,13.411,0,0,0,93.583,60.8l1.751,3.128H78.821a1.924,1.924,0,0,1-1.877-1.877V28.4a1.924,1.924,0,0,1,1.877-1.877H98.212a1.924,1.924,0,0,1,1.877,1.877ZM96.711,66.3l5.63,9.883V88.072H89.955V66.3Zm-42.66,47.039a3.264,3.264,0,0,1-2.127.876c-1.376,0-2.127-.751-3-1.751a6.19,6.19,0,0,0-4.879-2.5c-2.5,0-3.753,1.376-4.879,2.5-.876,1-1.626,1.751-3,1.751s-2.127-.751-3-1.751a6.19,6.19,0,0,0-4.879-2.5c-2.5,0-3.753,1.376-4.879,2.5-.876,1-1.626,1.751-3,1.751a2.522,2.522,0,0,1-1.376-.375A51.36,51.36,0,0,1,16.9,101.458a5.178,5.178,0,0,0,3.5,1.251c2.5,0,3.753-1.376,4.879-2.5.876-1,1.626-1.751,3-1.751s2.127.751,3,1.751a6.19,6.19,0,0,0,4.879,2.5c2.5,0,3.753-1.376,4.879-2.5.876-1,1.626-1.751,3-1.751s2.127.751,3,1.751a6.19,6.19,0,0,0,4.879,2.5,5.391,5.391,0,0,0,2.127-.375v11.009Zm15.263,25.146L78.07,149a50.681,50.681,0,0,1-8.757.876Zm0-4V120.849L89.7,145.119a53.352,53.352,0,0,1-8.882,3.128Zm0-17.514V116.1H80.072l19.391,23.144a77.243,77.243,0,0,1-7.506,4.879ZM82.074,90.7h6.255l25.646,30.65a46.087,46.087,0,0,1-4.879,7.881L82.074,96.954Zm9.383,0h11.509l15.137,18.14a51.6,51.6,0,0,1-3,10.008Zm14.762,0H119.1v8.507a54.56,54.56,0,0,1-.375,6.38Zm9.258-80.066H72.941C78.7,7.506,88.954,2.5,94.209,2.5,99.338,2.5,109.721,7.506,115.476,10.634ZM28.4,21.142h-.375A31.917,31.917,0,0,0,9.014,28.023V12.01c6.63-2,14.762-4.379,24.52-4.379l-.876,9.883,2.5.25.751-10.008h1a32.452,32.452,0,0,1,24.02,14.762l-7.756,5,1.376,2.127,7.631-5A29.789,29.789,0,0,1,65.81,39.783a127.11,127.11,0,0,1-2.377,21.142c-.375,1.626-.626,3-.876,4.379l-2.877-.5-.375,2.5,2.877.5a58.233,58.233,0,0,0-.626,9.132L54.051,73.56C55.677,70.933,58.8,64.3,59.18,52.168a29.87,29.87,0,0,0-2.752-13.136l4.253-2.127-1.126-2.252L55.3,36.905a28.51,28.51,0,0,0-4.754-6.255,32.428,32.428,0,0,0-11.635-7.631l.876-7.881-2.5-.25-.876,7.381A27.523,27.523,0,0,0,28.4,21.142Zm10.258,4.5A29.993,29.993,0,0,1,48.8,32.4a27.982,27.982,0,0,1,4.379,5.755l-1.877,1A26.653,26.653,0,0,0,38.538,27.9ZM28.4,28.273A23.759,23.759,0,0,1,52.174,52.043a23.407,23.407,0,0,1-4.253,13.511,6.875,6.875,0,0,1-.876,1.126,13.965,13.965,0,0,0-.125,15.012l-3.753-.25a2.509,2.509,0,0,1-2.5-2.5v-.25l-.876-4.879h0l-2-10.884c.125,0,.125-.125.25-.125.125-.125.375-.25.5-.375a2.2,2.2,0,0,0,.626-.5l.375-.375.5-.5a1.727,1.727,0,0,0,.375-.5,2.2,2.2,0,0,1,.5-.626c.125-.125.25-.375.375-.5a2.735,2.735,0,0,1,.375-.626c.125-.125.125-.375.25-.5a1.778,1.778,0,0,0,.25-.751c0-.125.125-.375.125-.5.125-.25.125-.626.25-.876,0-.125.125-.25.125-.375,0-.5.125-.876.125-1.376a9.825,9.825,0,0,0-9.883-9.883h-.876a9.17,9.17,0,0,0-4.754,1.751,25.133,25.133,0,0,1-5.63-5.5,19.256,19.256,0,0,0-8.882-6.505A23.172,23.172,0,0,1,28.4,28.273ZM25.277,52.918a7.893,7.893,0,0,1,2.5-4.128,7.435,7.435,0,0,1,4.754-1.751,7.316,7.316,0,0,1,7.381,7.381,2.439,2.439,0,0,1-.125,1,7.632,7.632,0,0,1-4.128,5.63,7.374,7.374,0,0,1-10.509-6.63A6.611,6.611,0,0,1,25.277,52.918Zm3.5,13.636.375-2.627c.125,0,.25,0,.25.125.25.125.626.125.876.25.125,0,.25.125.5.125.375,0,.751.125,1,.125h.626a4.693,4.693,0,0,0,1.251-.125h.125a3.653,3.653,0,0,0,1.126-.25h.25l1.877,10.008-2,4.253-7.381-4Zm-8.257,12.26c-5.254,0-9.383-1.376-12.26-4a13.25,13.25,0,0,0,1.251-1.5,41.085,41.085,0,0,0,2.5-3.878l.125-.25A32.681,32.681,0,0,0,16.02,53.419v-1a1.33,1.33,0,0,0-1.126-1.251A28.567,28.567,0,0,1,4.76,48.29c-.626-.375-1.376-.751-2-1.126A29.216,29.216,0,0,1,7.513,35.654a17.443,17.443,0,0,1,12.01,6.63,36.343,36.343,0,0,0,5.63,5.63c-.125.125-.125.125-.125.25l-.25.25c-.125.25-.25.375-.375.626,0,.125-.125.125-.125.25-.125.25-.375.626-.5.876v.125l-.375.751a.437.437,0,0,1-.125.25,1.128,1.128,0,0,0-.125.626.46.46,0,0,1-.125.375,5.418,5.418,0,0,1-.125.626v1.251a4.256,4.256,0,0,0,.125,1.251.46.46,0,0,0,.125.375c0,.25.125.5.125.876,0,.125.125.25.125.5.125.25.125.5.25.751.125.125.125.25.25.5a2.735,2.735,0,0,0,.375.626c.125.125.125.25.25.5s.25.375.375.626l.375.375c.125.125.25.375.5.5l.375.375.5.5.375.375c.125,0,.125.125.25.125L25.527,73.936c0,.125-.125.25-.125.375a.46.46,0,0,1-.125.375,1.808,1.808,0,0,1-.25.876C24.026,77.564,22.4,78.815,20.523,78.815Zm11.134.751,2.5,1.376a2.357,2.357,0,0,0,1,.25.937.937,0,0,0,.626-.125,2.2,2.2,0,0,0,1.251-1.126l.876-1.751.25,1a4.993,4.993,0,0,0,5,4.879h0l6.38.375h.125a.46.46,0,0,0,.375-.125,1.511,1.511,0,0,0,.876-.876,1.2,1.2,0,0,0-.25-1.126,11.69,11.69,0,0,1-3.128-8.007,11.129,11.129,0,0,1,1.877-6.255,5.221,5.221,0,0,1,.751-1,25.857,25.857,0,0,0,2.377-25.771l2-1a26.574,26.574,0,0,1,2.5,12.01C56.177,67.305,51.3,73.31,51.3,73.435a1.2,1.2,0,0,0-.25,1.126,1.323,1.323,0,0,0,.751.876l10.884,4.754a.752.752,0,0,0,.5.125,1.778,1.778,0,0,0,.751-.25,1.451,1.451,0,0,0,.5-1.251c-.25-1.251-.876-6,1.5-17.264.375-1.626.626-3.128.876-4.629V88.2H31.657ZM54.051,90.7v8.632a3.264,3.264,0,0,1-2.127.876c-1.376,0-2.127-.751-3-1.751a6.19,6.19,0,0,0-4.879-2.5c-2.5,0-3.753,1.376-4.879,2.5a3.309,3.309,0,0,1-2.877,1.626c-1.376,0-2.127-.751-3-1.751a6.19,6.19,0,0,0-4.879-2.5c-2.5,0-3.753,1.376-4.879,2.5-.876,1-1.626,1.751-3,1.751s-2.127-.751-3-1.751l-.626-.626V90.574H54.051ZM25.9,127.73a3.266,3.266,0,0,1,2.5-1.251c1.376,0,2.127.751,3,1.751a6.19,6.19,0,0,0,4.879,2.5c2.5,0,3.753-1.376,4.879-2.5.876-1,1.626-1.751,3-1.751s2.127.751,3,1.751a6.19,6.19,0,0,0,4.879,2.5c2.5,0,3.753-1.376,4.879-2.5.876-1,1.626-1.751,3-1.751s2.127.751,3,1.751a6.211,6.211,0,0,0,4,2.5v11.509a5.446,5.446,0,0,1-2.252-1.626,6.19,6.19,0,0,0-4.879-2.5c-2.5,0-3.753,1.376-4.879,2.5-.876,1-1.626,1.751-3,1.751s-2.127-.751-3-1.751a6.19,6.19,0,0,0-4.879-2.5c-2.5,0-3.753,1.376-4.879,2.5l-.375.375A56.433,56.433,0,0,1,25.9,127.73ZM41.04,142.242h0c1-1,1.626-1.751,3-1.751s2.127.751,3,1.751a6.19,6.19,0,0,0,4.879,2.5c2.5,0,3.753-1.376,4.879-2.5.876-1,1.626-1.751,3-1.751s2.127.751,3,1.751a6.211,6.211,0,0,0,4,2.5V150A53.181,53.181,0,0,1,41.04,142.242Zm60.3-4.629L82.074,114.469V100.833l25.521,30.4A50.239,50.239,0,0,1,101.339,137.613Z" transform="translate(0.025)"/>
                                        <path d="M86.813,40.482a4.688,4.688,0,0,0-6.63,6.63,4.689,4.689,0,0,0,6.63-6.63Zm-1.751,4.879a2.212,2.212,0,0,1-3.128-3.128,2.213,2.213,0,0,1,3.128,3.128Z" transform="translate(19.806 9.809)"/>
                                        <rect width="4.629" height="2.502" transform="translate(77.47 18.89)"/>
                                        <path d="M49.7,85a2,2,0,1,0,2,2A1.976,1.976,0,0,0,49.7,85Z" transform="translate(12.006 21.337)"/>
                                        <path d="M54.8,80a2,2,0,1,0,2,2A1.976,1.976,0,0,0,54.8,80Z" transform="translate(13.286 20.082)"/>
                                        <path d="M59.9,85a2,2,0,1,0,2,2A1.976,1.976,0,0,0,59.9,85Z" transform="translate(14.566 21.337)"/>
                                        <path d="M49.7,75a2,2,0,1,0,2,2A1.976,1.976,0,0,0,49.7,75Z" transform="translate(12.006 18.827)"/>
                                        <path d="M59.9,75a2,2,0,1,0,2,2A1.976,1.976,0,0,0,59.9,75Z" transform="translate(14.566 18.827)"/>
                                        <path d="M71.3,50.1a2,2,0,1,0-2-2A1.976,1.976,0,0,0,71.3,50.1Z" transform="translate(17.428 11.572)"/>
                                    </svg>
                                </div>
                            )}

                            {history_item.images.length > 0 && (
                                <img
                                    className="town_question_image"
                                    src={`${FG_BASE_URL}${history_item.images[0]}`}
                                    alt=""
                                />
                            )}
                            
                        </div>

                        <div className="town_title_wrapper">
                            <h5>{history_item.title}</h5>
                            <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="9.5"
                                    height="17"
                                    viewBox="0 0 9.5 17"
                                    className="arrow_left_icon"
                                    >
                                    <path
                                        d="M8,.5.5,8,8,15.5"
                                        transform="translate(0.5 0.5)"
                                        />
                            </svg>
                        </div>
                    </button>
                ))}
            </section>

            {typeof selectedHistory !== "undefined" && (
                <Dialog
                    isOpen={showDialog}
                    onDismiss={() => setShowDialog(false)}
                >
                    <button
                        className="close-button"
                        onClick={() => setShowDialog(false)}
                    >
                        <VisuallyHidden>Close</VisuallyHidden>
                        <span aria-hidden>×</span>
                    </button>

                    {selectedHistory.images.length === 1 && (
                        <div className="swiper_image">
                            <img
                                src={`${FG_BASE_URL}${selectedHistory.images[0]}`}
                                alt=""
                            />
                        </div>
                    )}

                    {selectedHistory.images.length > 1 && (
                        <Swiper {...swiperParams}>
                            {selectedHistory.images.map((image, index) => (
                                <div key={index}>
                                    <img
                                        src={`${FG_BASE_URL}${image}`}
                                        alt=""
                                    />
                                    <label className="image_credits">{selectedHistory.virtulleum_credits[index]}</label>
                                </div>
                            ))}
                        </Swiper>
                    )}

                    <section className="dialog_description">
                        <h3>{selectedHistory.title}</h3>
                        <h5 className="history_description">{selectedHistory.description}</h5>
                        <div
                            dangerouslySetInnerHTML={{
                                __html:
                                    selectedHistory.virtulleum_website_description_more
                            }}
                        ></div>
                        
                        {selectedHistory.virtulleum_pdf.length > 0 && (
                            <section className="pdf_links_section">
                                <h3>{translations.other_info[language_id]}</h3>
                                {selectedHistory.virtulleum_pdf.map((link, index) => (   
                                   
                                    <a key={index} href={`${FG_BASE_URL}${link}`} target="_blank" className="pdf_link">{ createLinkName(link) }</a>
                                    
                                ))}
                        </section>
                        )}

                    </section>
                </Dialog>
            )}
        </>
    );
};

export default HistoryGrid;



const HISTORY_DATA = graphql`
    query HistoryData {
        allTownHistoryList {
            nodes {
              title
              virtulleum_website_description_more
              description
              alternative_id
              virtulleum_website_url
              images
              virtulleum_pdf
              virtulleum_website_tags
              sys_languages_id
              virtulleum_public_number
              virtulleum_credits
            }
        }
    }
`